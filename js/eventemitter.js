//contructor
function EventEmitter() {
	this._events = {};
}

//crear
EventEmitter.prototype.on = function(type, listener) {
	//se inicializa el objeto
	//type sera solo el nombre del event emitter
	//listener es 
	this._events[type] = this._events[type] || [];
	this._events[type].push(listener);
};
//emitir
EventEmitter.prototype.emit = function(type) {
	if(this._events[type]) {
		this._events[type].forEach(function(listener){
			listener();
		});
	}
};
//quitar
EventEmitter.prototype.removeListener = function(type, listener) {
	if(this._events[type]){
		this._events[type].splice(this._events[type].indexOf(listener),1)
		//splice elimina un elemento del array
		//buscara la funcion en particular para eliminar
	}
};