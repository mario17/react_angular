var Forum = React.createClass({
	//inicializo el objeto inicial
	getInitialState: function(){
		return{
			allAnswers:{
				"1":{
					body:"Es acerca de un viaje",
					correct:false
				},
				"2":{
					body:"Esuna herramienta para hacer frontend",
					correct:false
				},
				"3":{
					body:"Es sinonimos de response",
					correct:false
				}
			}
		}
	},
	render: function(){
		//REMEMBER: Cerrar bien etiquetas
		//creara un nuevo component
		return(
			<div>
				<ForumHeader />
				<div className="container">
					<ForumQuestion />
					<hr/>
					<ForumAnswers allAnswers={this.state.allAnswers} />
					<hr/>
					<h4> Add an answer </h4>
					<ForumAddAnswerBox onAddAnswer={ this._onAddAnswer } />
				</div>

				<ForumFooter />
			</div>
			
			//se le pasa el objeto allAnswer
			);
	},
	_onAddAnswer:function(answerText){
		ForumDispatcher.dispatch({
			actionType: 'FORUM_ANSWER_ADDED',
			newAnswer: answerText
		});
	}
});