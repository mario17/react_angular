//Dispatcher chism le dice a ala que ha ocurrido algo
/*
	FAKE EVENT LISTNEING. MORE OBJECTS AND FUNCTIONS.

	Funciones que responden a una particular ACTION**

	SON OBJETOS QUE CONTIENE UNICOS VALORES que identifican la accion
	y contienen datos asociados con la accion
*/
/*
	Una accion en particular podria ser una nueva respuesta
	marcar una opcion como correcta, es una accion
	*/
/* SOLO HAY UN DISPATCHER*/

function Dispatcher() {
	//constructor con una propiedad
	this._lastID = 0;
	this._callbacks = {};
}

Dispatcher.prototype.register = function (callback) {
	//por el callback conseguire el id
	var id = 'CID_' + this._lastID++;
	this._callbacks[id] = callback;
	return id;
};
Dispatcher.prototype.dispatch = function (action) {
	for (var id in this._callbacks) {
		this._callbacks[id](action);
	}
};

/*Dispatcher.prototype.waitFor = function(ids){
	//TODO
};*/