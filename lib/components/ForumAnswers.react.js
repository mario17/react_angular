var ForumAnswers = React.createClass({
	displayName: 'ForumAnswers',


	_onMarkCorrect: function (id) {
		ForumDispatcher.dispatch({
			actionType: 'FORUM_ANSWER_MARKED_CORRECT',
			id: id
		});
	},
	render: function () {
		var allAnswers = this.props.allAnswers;
		var answers = [];

		for (var key in allAnswers) {
			answers.push(React.createElement(ForumAnswer, { key: key, id: key, onMarkCorrect: this._onMarkCorrect, answer: allAnswers[key] }));
		}

		return React.createElement(
			'div',
			null,
			answers
		);
	}
});