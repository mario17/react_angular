var Forum = React.createClass({
	displayName: "Forum",

	//inicializo el objeto inicial
	getInitialState: function () {
		return {
			allAnswers: {
				"1": {
					body: "Es acerca de un viaje",
					correct: false
				},
				"2": {
					body: "Esuna herramienta para hacer frontend",
					correct: false
				},
				"3": {
					body: "Es sinonimos de response",
					correct: false
				}
			}
		};
	},
	render: function () {
		//REMEMBER: Cerrar bien etiquetas
		//creara un nuevo component
		return React.createElement(
			"div",
			null,
			React.createElement(ForumHeader, null),
			React.createElement(
				"div",
				{ className: "container" },
				React.createElement(ForumQuestion, null),
				React.createElement("hr", null),
				React.createElement(ForumAnswers, { allAnswers: this.state.allAnswers }),
				React.createElement("hr", null),
				React.createElement(
					"h4",
					null,
					" Add an answer "
				),
				React.createElement(ForumAddAnswerBox, { onAddAnswer: this._onAddAnswer })
			),
			React.createElement(ForumFooter, null)
		)

		//se le pasa el objeto allAnswer
		;
	},
	_onAddAnswer: function (answerText) {
		ForumDispatcher.dispatch({
			actionType: 'FORUM_ANSWER_ADDED',
			newAnswer: answerText
		});
	}
});