var ForumAnswer = React.createClass({
	displayName: "ForumAnswer",

	propTypes: {
		onMarkCorrect: React.PropTypes.func.isRequired,
		answer: React.PropTypes.object.isRequired
		//es requerido que le pasen answer
	},
	_markCorrect: function () {
		this.props.onMarkCorrect(this.props.id);
	},
	render: function () {
		var answer = this.props.answer;
		return React.createElement(
			"div",
			{ className: "panel panel-default" },
			React.createElement(
				"div",
				{ className: "panel-body" },
				answer.body,
				React.createElement(
					"div",
					{ className: "pull-right" },
					React.createElement(
						"small",
						null,
						React.createElement(
							"a",
							{ href: "#", onClick: this._markCorrect },
							"Mark as correct"
						)
					)
				)
			)
		);
	}
});